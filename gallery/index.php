<?php include('../perch/runtime.php'); ?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog Home - Start Bootstrap Template</title>

  <?php perch_layout('global.header.css'); ?>

  	
  </style>


  </head>
<body>

  <?php perch_layout('global.header.nav'); ?>

    <!-- Page Content -->
    <div class="container">

      <h1 class="my-4">Thumbnail Gallery</h1>

      <div class="row">

		    <?php 
		        perch_gallery_albums(array(
		            'template'=>'a_album-image.html',
		            'image'=>true
		        )); 
		    ?>
  


  
      </div>

    </div>



		  



 <?php perch_layout('global.footer'); ?>

<?php perch_layout('global.footer.js'); ?>


  </body>

</html>
