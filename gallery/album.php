<?php include('../perch/runtime.php'); ?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog Home - Start Bootstrap Template</title>

  <?php perch_layout('global.header.css'); ?>
  <link href="/css/lightbox.css" rel="stylesheet">

  </head>
<body>

  <?php perch_layout('global.header.nav'); ?>

    <!-- Page Content -->
    <div class="container">

      <h1 class="my-4 text-center text-lg-left"><?php perch_gallery_album_field(perch_get('s'), 'albumTitle'); ?> </h1>

      <div class="row text-center text-lg-left">

 			<?php 
			if(perch_get('s')) {

                // Output the large images
				perch_gallery_album_images(perch_get('s'), array(
            	   'template'   =>'a_list_image.html'
            	));
				
				// Output the small images used for navigation
				perch_gallery_album_images(perch_get('s'), array(
            	   'template'   =>'a_nav_image.html',
            	));
			}
			 
			?>
  

      </div>

    </div>
    <!-- /.container -->

	







 <?php perch_layout('global.footer'); ?>

<?php perch_layout('global.footer.js'); ?>

 <script src="/js/lightbox.js"></script>
 <script>
 	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
 </script>
  </body>

</html>




	
	


