<?php include('../perch/runtime.php'); ?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Contact Home - Start Bootstrap Template</title>

  <?php perch_layout('global.header.css'); ?>

  	
  </style>


  </head>
<body>

  <?php perch_layout('global.header.nav'); ?>

    <!-- Page Content -->
    <div class="container">

<h1 class="my-4">Get in touch</h1>
                         <p>Lorem ipsum dolor sit amet, consectetur venenatis tincidunt.</p>


      <div class="row">


               <div class="col-md-7 col-sm-12">

              <?php perch_content('contactForm'); ?>
             
               </div>

               <div class="col-md-5 col-sm-12">
                    <!-- CONTACT INFO -->
                
                         <div class="section-title">
                              <h2>Contact Info</h2>
                              <p>Lorem ipsum dolor sit consectetur adipiscing morbi venenatis et tortor consectetur adipisicing lacinia tortor morbi ultricies.</p>
                         </div>
                         
                         <p>456 New Street 22000, New York City, USA</p>
                         <p><a href="mailto:info@company.com">info@company.com</a></p>
                         <p>010-020-0340</p>
            
               </div>

  
      </div>

    </div>



		  



 <?php perch_layout('global.footer'); ?>

<?php perch_layout('global.footer.js'); ?>


  </body>

</html>
