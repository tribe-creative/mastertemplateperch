<?php include('../perch/runtime.php'); ?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog Home - Start Bootstrap Template</title>

  <?php perch_layout('global.header.css'); ?>

    <link rel="stylesheet" href="blog.css" type="text/css" />

  </head>

  <body>

  <?php perch_layout('global.header.nav'); ?>

    <!-- Page Content -->
    <div class="container">
      <div class="row mb-4">

        <!-- Blog post Column -->
	
		    	<?php perch_blog_post(perch_get('s')); ?>
		   

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

          <!-- Search Widget -->
        <?php perch_search_form(); ?>

          <!-- Categories Widget -->
            <?php perch_blog_categories(); ?>
            
            <?php perch_blog_tags(); ?>
        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->


  

 <?php perch_layout('global.footer'); ?>

<?php perch_layout('global.footer.js'); ?>

  </body>

</html>




	
	
